package org.nljug.lrm.central.mqtt;

import io.github.giovibal.mqtt.parser.MQTTDecoder;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.dna.mqtt.moquette.proto.messages.AbstractMessage;
import org.dna.mqtt.moquette.proto.messages.PublishMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MQTTEventBridge extends AbstractVerticle {

    Logger logger = LoggerFactory.getLogger(MQTTEventBridge.class);

    MQTTDecoder mqttDecoder = new MQTTDecoder();

    @Override
    public void start() throws Exception {

        vertx.eventBus().consumer("io.github.giovibal.mqtt", event -> {

            try {
                AbstractMessage message = mqttDecoder.dec((Buffer) event.body());

                if (message instanceof PublishMessage) {
                    PublishMessage publishMessage = (PublishMessage)message;

                    String topic = publishMessage.getTopicName();
                    String address = "lrm.events." +  topic;


                    JsonObject payload = new JsonObject();
                    payload.put("topic", topic);
                    payload.put("payload", new String(publishMessage.getPayload().array()));


                    logger.info("Publishing to: {}, payload: {}", address, payload);
                    vertx.eventBus().publish(address, payload);


                } else {
                    logger.warn("Unsupported MQTT message type: {}", message.getClass().getName());
                }
            } catch (Exception e) {
                logger.error("Could not decode MQTT Message, reason: {}", e.getMessage(), e);
            }

        });
    }
}
