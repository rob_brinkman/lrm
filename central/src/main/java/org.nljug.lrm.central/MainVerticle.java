package org.nljug.lrm.central;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainVerticle extends AbstractVerticle {

    Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start() throws Exception {
        super.start();

        logger.debug("Config: {}", config().toString());
        if (config().isEmpty()) {
            throw new IllegalArgumentException("Please pass a valid config file using -conf <configfile>");
        }

        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(config().getJsonObject("mqtt"));
        vertx.deployVerticle("io.github.giovibal.mqtt.MQTTBroker", deploymentOptions, deploymentResult -> {
            if (deploymentResult.succeeded()) {
                logger.info("Deployed MQTT broker module");
            } else {
                logger.error("Could not deploy MQTT broker module, reason: {}", deploymentResult.cause().getMessage(), deploymentResult.cause());
            }
        });

        vertx.deployVerticle("org.nljug.lrm.central.mqtt.MQTTEventBridge", deploymentResult -> {
            if (deploymentResult.succeeded()) {
                logger.info("Deployed MQTT Event Bridge module");
            } else {
                logger.error("Could not deploy MQTT Event Bridge module, reason: {}", deploymentResult.cause().getMessage(), deploymentResult.cause());
            }
        });
    }
}
