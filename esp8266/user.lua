function loadConfiguration(configFile) 
    if file.open(configFile) then
        file.close() 
        dofile(configFile)
    else
        error(configFile .. " not found!!!!")
    end
end

function enableSensorAndLed(callback) 
    local sensorpin = 2
    local ledpin = 3
     
    gpio.mode(sensorpin, gpio.INT)
    gpio.mode(ledpin, gpio.OUTPUT)gpio.mode(ledpin, gpio.OUTPUT)
     
    gpio.trig(sensorpin, "both",  function ()
        local motion_value = gpio.read(sensorpin)
        gpio.write(ledpin, motion_value);   
        if motion_value == 1 then 
            callback(true)
            print("Motion detected")
        else 
            print("No motion") 
            callback(false)
        end   
    end)   
end

function connectWifi (callback) 
    node.restore() 
    wifi.setmode(wifi.STATION)
    wifi.sta.config(wifi_ssid,wifi_password, 1) -- don't use auto connect we will control the connection
    wifi.sta.connect()

    tmr.alarm(1, 2000, 1, function() 
        status = wifi.sta.status()
        if (status ~= 5) then
            print("Waiting for WIFI to connect to " .. wifi_ssid ..", current status: " .. status)
        else
            print("Wifi connected: " .. wifi.sta.getip())
            tmr.stop(1)
            callback()
        end 
    end)
end

function connectMQTT() 
    
    -- init mqtt client with keepalive timer 120sec
    m = mqtt.Client(mqtt_clientid, 120, nil, nil)
    
    -- register event handlers
    m:on("connect", function(client) 
        print ("mqtt connected") 
        tmr.stop(2)
    end)

    m:on("offline", function(client) 
        print ("MQTT disconnected, reconnecting")
        tmr.alarm(2, 2000, 1, function()
            connectMQTT()
        end)
         
    end)

    m:on("message", function(client, topic, data) 
        if data ~= nil then
            print(data)
        end
    end)
       
    -- connect to broker
    print("Connecting mqtt")
    m:connect(mqtt_host, mqtt_port)
end

function enableTelnet() 
    local telnetFile = "telnet.lc"
    print("Enabling telnet using " .. telnetFile)
    if file.open(telnetFile) then
        file.close() 
        dofile(telnetFile)
    else
        error(telnetFile " .. not found, did you compile the telnet.lua?")
    end
end

loadConfiguration("settings.lua")
connectWifi(function()
    connectMQTT()
    enableTelnet()
end)

enableSensorAndLed(function(motionDetected)    
    if m ~= nil then 
        m:publish( "motion", tostring(motionDetected), 0, 0, function(client)
            print("Event published to mqtt")
        end)
    end
end)
